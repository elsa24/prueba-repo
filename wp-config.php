<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'truvia2016');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'truvia2016');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'truvia2016');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'h{#$zX&4%O6g<Oi;4-$!TYAV `e>oDC`YEoDMe3lY-Y3m6RRSs3[N<5u_ :S@kq8');
define('SECURE_AUTH_KEY', 'T>jgy5k_lMQODAB=3=:).mq!pI{yy=L[N449q6YlPf;BSkuFRF0Fa{/r, t,Q/R]');
define('LOGGED_IN_KEY', '`+osrn`RmW}+4+0/2$9KSkK[h(&<$dJi^74m_/$e.WqLOY!?~TW$~{xO.rR]Ksf2');
define('NONCE_KEY', '^p55sy=hSa-Y>4ZQ: @dU{p,m]S=TTednyQfz#]2*U/mE{%^#?SasYsk88.|Gbhz');
define('AUTH_SALT', '~}O8QC(:Q;a]{R^k8Tk!$b#HGR9QI2RK.Z{`lE0uA1/>&yHf#Z31K:{az_Bo, Kf');
define('SECURE_AUTH_SALT', 'sJ)U?BpIuSe8l/-Vc@g@u4EP3r`ZVU5+RCD}vcl;LjQR/<^_&rrTglC-)uqzKY=h');
define('LOGGED_IN_SALT', '9;;xK3G`=Tpf5@%gCQh=uF_x7lem(kZv>|0K~I{;3;rwHS,cm|bIyYgI1wAKdI-a');
define('NONCE_SALT', '(P7^#:.+t1@RRZo!)g)-wMM |E JATxczsEcv!M?lhDru9lYE&_X[|p&||?H{ +Y');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', true);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
/*Ht2IhhQH*rKySf(!Tt*/
require_once(ABSPATH . 'wp-settings.php');

