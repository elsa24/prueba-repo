 <footer id="footer">
            <div class="container">
                <div class="row">
                    <span class="hidden-xs">
                <div class="col-sm-4 col-md-2">
        <ul class="footer__nav">
            <li class="category"><a class="category__parent-link" href="//www.truvia.mx/acerca">Acerca de Truvía<sup>®</sup></a>
                <ul class="category--children">
                    <li class="category__child"><a href="//www.truvia.mx/acerca#stevia">STEVIA</a></li>
                    <li class="category__child"><a href="//www.truvia.mx/acerca/compania">COMPAÑIA</a></li>
                    <li class="category__child"><a href="//www.truvia.mx/acerca/compania#media">PRENSA</a></li>
                </ul>
            </li>
        </ul>
    </div>

<div class="col-sm-4 col-md-2">
        <ul class="footer__nav">
            <li class="category"><a class="category__parent-link" href="//www.truvia.mx/recetas">Recetas</a>
                <ul class="category--children">
                    <li class="category__child"><a href="//www.truvia.mx/tabla-de-conversion">TABLAS DE CONVERSIÓN</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="col-sm-4 col-md-2">
        <ul class="footer__nav">
            <li class="category">
                        <a class="category__parent-link" href="//www.truvia.mx/productos">Producto</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-2">
                <ul class="footer__nav">
                    <li class="category">
                        <a class="category__parent-link" href="//www.truvia.mx/dietetico">Cuidados Profesionales</a>
                        <ul class="category--children">
                            <li class="category__child"><a href="//www.truvia.mx/dietetico">INFORMACIÓN DIETÉTICA</a></li>
                            <li class="category__child"><a href="//www.truvia.mx/dietetico/investigacion">Investigación y Resultados</a></li>
                            <li class="category__child"><a href="//www.truvia.mx/dietetico/qa">FAQ</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-2">
                <ul class="footer__nav"></ul>
    </div>
        </span>
                    <div class="col-sm-4 col-md-2">
                        <ul class="footer__nav--last">
                            <li class="category--last">
                                <a class="category__parent-link" href="//www.truvia.mx/faq">FAQ</a>
                            </li>
                            <li class="category--last">
                                <a class="category__parent-link" href="//www.truvia.mx/contacto">Contacto</a>
                            </li>
                        </ul>
                    </div>


                    <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4 col-md-offset-0 col-md-2 vs-sm-top-4 vs-md-top-0">
                        <ul>
                            <li class="footer__social">
                                <span class="hidden-xs">Síguenos:</span>
                                <ul>
                                    <li>
                                        <a data-label="" href="https://www.facebook.com/TruviaMx" target="_blank"><img class="no-drag" src="<?=get_template_directory_uri(); ?>/images/footer-facebook-icon.png" alt="footer-facebook-icon"></a>
                                    </li>
                                    <li>
                                        <a data-label="" href="https://twitter.com/truviamx" target="_blank"><img class="no-drag" src="<?=get_template_directory_uri(); ?>/images/footer-twitter-icon.png" alt="footer-twitter-icon"></a>
                                    </li>
                                    <li>
                                        <a data-label="" href="https://www.pinterest.com/truviamx/" target="_blank"><img class="no-drag" src="<?=get_template_directory_uri(); ?>/images/footer-pinterest-icon.png" alt="footer-pinterest-icon"></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">


                        <?php wp_nav_menu(array(

                                'menu' => 'menu-footer',
                                'menu_class' => 'footer__secondary',
                                'menu-item' => '<li class="hidden-sm hidden-xs"><span>©2016 The Truvía<sup>®</sup> Company, LLC</span></li>'


                            ));?>

                            

                        <!--<ul class="footer__secondary">
                            <li><a href="//www.truvia.mx/privacidad" title="Política de Privacidad ">Políticas de Privacidad </a></li>
                            <li><a href="//www.truvia.mx/terminos" title="Términos de Uso">Términos de Uso</a></li>
                            <li><a href="http://www.cargill.com" title="Cargill" target="_blank">Cargill</a></li>
                            <li class="hidden-sm hidden-xs"><span>©2016 The Truvía<sup>®</sup> Company, LLC</span></li>
                        </ul>-->
                    </div>
                </div>
            </div>
        </footer>

    </div>
   <?php wp_footer();?>
</body>

</html>