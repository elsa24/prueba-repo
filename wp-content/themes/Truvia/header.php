<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" class="offcanvas-right no-touchevents">
<?php wp_head(); ?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Truvía</title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
</head>

<body>
    <div class="offcanvas-sidenav-sibling">

        <header id="header">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button id="toggle" type="button" class="navbar-toggle collapsed" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a class="navbar-brand" href="//www.truvia.com/">
                            <img height="41" src="<?=get_template_directory_uri(); ?>/images/truvia_logo_navbar.png" alt="Truvía®" data-pin-description="Truvía®">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse">
                        <?php /*wp_nav_menu(array(
                            'container' => false,
                            'items_wrap' => '<ul id="menu" ></ul>',
                            'theme_location' => 'menu'));*/
                            ?>
                            <ul>
                            <?php /*wp_nav_menu(array(
                            'container' => false,    // para que no tenga contenedor
                            'menu_id' => 'menu-principal',    // id del menu
                            'link_before' => '<span>', // HTML previo al texto de cada sección
                            'link_after' => '</span>'    // HTML posterior al texto de cada sección
                            ));*/ ?>
                        </ul>
                            <?php wp_nav_menu(array(

                                'menu' => 'menu-header',
                                'menu_class' => 'nav navbar-nav navbar-main'


                            ));?>


                        <!--<ul class="nav navbar-nav navbar-main">
                            <li><a href="https://www.truvia.mx/acerca" title="Acerca de Truvía®">Nosotros</a></li>
                            <li><a href="https://www.truvia.mx/recetas" title="Recipes">Recetas</a></li>
                            <li><a href="https://www.truvia.mx/productos" title="Truvia Products: Reviews, Locator &amp; Partners">Producto</a></li>
                        </ul>-->

                        <!--<ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle en_us" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">region</a>
                                <ul class="truvia-bold dropdown-menu languages">
                                    
                                    <li>
                                        <a href="//www.truvia.com/canada/" class="en_ca">Canada (English)</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.com/canada-fr/" class="fr_ca">Canada (Français)</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.com.es/" class="es_es">España</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.fr/" class="fr_fr">Français</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.it/" class="it_it">Italia</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.mx/" class="es_mx">Mexico</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.co.uk/" class="en_gb">United Kingdom</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.com/health/blog" class="en_us">United States</a>
                                    </li>
                                    <li>
                                        <a href="//truvia.com.ve" class="es_ve" target="_blank">Venezuela</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>-->
                        
                        
                        
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
        </header>
