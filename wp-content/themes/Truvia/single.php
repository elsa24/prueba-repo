<?php get_header(); ?>
<main id="content" role="main">
    <div class="page-wrapper top-gradient">


        <div class="container">
            <div class="row">
                <!-- Main column -->

                

                <div class="col-sm-8">
                    <?php if(have_posts()) : while(have_posts()) : the_post();?> 

                    <div class="article-detail-date-author">
                        <strong>Julie Upton</strong> - May 4, 2016
                    </div>
                    
                    <!--redes sociales-->
                    <div class="vs-xs-bottom-3">
                        <!-- Load Facebook SDK for JavaScript -->
                        <div id="fb-root" class=" fb_reset">
                            <img src="https://www.facebook.com/rsrc.php/v2/yD/r/FEppCFCt76d.png">
                        </div>
            

                        <div>
                            <img src="">
                        </div>


                       

                        <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button"
                            title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.b41e99df00581dc95d7fdd63f3283511.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=https%3A%2F%2Fwww.truvia.com%2Fhealth%2Fblog%2Fthe-new-dietary-guidelines-and-added-sugars-how-to-help-patients-meet-the-updated-recommendations&amp;size=m&amp;text=The%20New%20Dietary%20Guidelines%20and%20Added%20Sugars%3A%20How%20to%20Help%20Patients%20Meet%20the%20Updated%20Recommendations%20&amp;time=1468959048247&amp;type=share&amp;url=https%3A%2F%2Fwww.truvia.com%2Fhealth%2Fblog%2Fthe-new-dietary-guidelines-and-added-sugars-how-to-help-patients-meet-the-updated-recommendations"
                            style="position: static; visibility: visible; width: 60px; height: 20px;"></iframe>



                        <span class="PIN_1468959048383_button_pin" data-pin-log="button_pinit_bookmarklet" data-pin-href="https://www.pinterest.com/pin/create/button/"></span>

                        <a class="social-share--email" href="mailto:?subject=The%20New%20Dietary%20Guidelines%20and%20Added%20Sugars%3A%20How%20to%20Help%20Patients%20Meet%20the%20Updated%20Recommendations%20&amp;body=[body]"
                            onclick="this.href = this.href.replace('[body]',window.location)" title="Email">
                            <span class="sr-only">Email</span>
                        </a>

                        <a class="social-share--print hidden-xs" href="javascript:window.print()" title="Print">
                            <span class="sr-only">Print</span>
                        </a>
                    </div>
                    
                     <!--redes sociales-->

                    <?php if( has_post_thumbnail()){ the_post_thumbnail('image-int');}  ?>

                    <div>
                        <p><?php the_content(); ?></p>
                    </div>

                    <!--<hr>
                    <h2>References: </h2>
                    <ol class="credit">
                        <li>
                            <p>U.S. Department of Health and Human Services and U.S. Department of Agriculture. 2015 – 2020
                                Dietary Guidelines for Americans. 8th Edition. December 2015. Available at <a href="http://health.gov/dietaryguidelines/2015/guidelines/">http://health.gov/dietaryguidelines/2015/guideline...</a>
                            </p>
                        </li>
                        <li>
                            <p>2015 Dietary Guidelines Advisory Committee (DGAC). Scientific Report of the 2015 Dietary Guidelines
                                Advisory Committee; Advisory Report to the Secretary of Health and Human Services and the
                                Secretary of Agriculture. First Print February 2015. Accessed on 19 Apr 2016. Retrieved from
                                <a href="http://health.gov/dietaryguidelines/2015-scientific-report/11-chapter-6/d6-3.asp">http://health.gov/dietaryguidelines/2015-scientifi...</a>
                            </p>
                        </li>
                        <li>
                            <p>World Health Organization Guideline: Sugars intake for adults and Children. 2015. Available at
                                <a href="http://apps.who.int/iris/bitstream/10665/149782/1/9789241549028_eng.pdf">http://apps.who.int/iris/bitstream/10665/149782/1/...</a>
                            </p>
                        </li>
                        <li>
                            <p>Yang Q, Zhang Z, Gregg EW, Flanders WD, Merritt R, Hu FB. Added sugar intake and cardiovascular
                                diseases mortality among US adults. JAMA Intern Med. 2014;174(4):516-24.
                            </p>
                        </li>
                        <li>
                            <p>Ng SW1, Slining MM, Popkin BM. Use of caloric and noncaloric sweeteners in US consumer packaged
                                foods, 2005-2009. J Acad Nutr Diet. 2012 Nov;112(11):1828-34.e1-6.
                            </p>
                        </li>
                    </ol>-->
                    <?php endwhile; else : ?>
                                
                                <h1>No hay entradas</h1>
                                
                                <?php endif; ?>

                </div>

                <!-- Sidebar -->
                <?php get_sidebar();?>













            </div>
        </div>

        
    </div>
</main>

<?php get_footer();?>