<?php
register_nav_menus(array(
	'menu-header' => 'menu-header',
	'menu-footer' => 'menu-footer'
	));


 add_theme_support('post-thumbnails');
 add_image_size('image-home',240,199,true);

 
 //sidebar
 register_sidebar(array(
     'name' => 'Sidebar',
     'id' => 'id-sidebar',
     'before_widget' => '<section class="well">',
     'after_widget' => '</section>',
     'before_title' => ' <h3 class="sidebar__title">',
     'after_title' => '</h3>'
             
 ));     
?>