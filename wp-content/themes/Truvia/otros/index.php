<?php 
get_header();

?>
<main id="content" role="main">
            <div class="page-wrapper top-gradient">
                <div class="container">
                    <div class="row">
                        <!-- Main column -->
                        <div class="col-sm-8">
                            <ul class="media-list media-list--blog">
                                
                                <?php query_posts("paged=$paged"); ?>
                                <?php if(have_posts()) : while(have_posts()) : the_post();?> 
                                <li class="media">
                                    <div class="media-left">
                                        <a href="<?php the_permalink(); ?>"
                                            title="<?php the_title(); ?>">
                                            
                                            <div><a href="<?php the_permalink();?>">
                                                <?php if( has_post_thumbnail()){ the_post_thumbnail('image-home');}  ?>
                                                </a></div>
                                            
                                            
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h4>

                                        <?php the_category();?><strong><?php echo get_the_date(); ?></strong>

                                        <p><?php the_excerpt(); ?></p>

                                        <a href="<?php the_permalink(); ?>"
                                            title="<?php the_title(); ?>"
                                            class="media__read">Read More »</a>
                                    </div>
                                </li>
                                <?php endwhile; else : ?>
                                
                                <h1>No hay entradas</h1>
                                
                                <?php endif; ?>
                                 
                               
                            </ul>

                            <div class="row">
                                <div class="col-xs-12 pagination-links">
                                    <?php next_posts_link("post siguientes >> ");?>
                                    <?php previous_posts_link("<< post anteriores");?>
                                    
                                   
                                </div>
                            </div>
                        </div>

                        <!-- Sidebar -->
                       <?php get_sidebar();?>
                        <!--termina-->

                    </div>
                </div>
                <div class="bottom-gradient"></div>
            </div>
        </main>

<?php 
get_footer();

?>
        