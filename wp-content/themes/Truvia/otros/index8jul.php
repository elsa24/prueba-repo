<?php
/*Template Name: Truvia */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Truvía®</title>
	 <link rel="stylesheet" type="text/css" href="style.css">
     <meta name="viewport" content="width=device-width">
</head>
<body>
	<!---->
	<div class="offcanvas-sidenav-container">
        <div class="offcanvas-sidenav">
            <img src="//d2sf2a832xqb4v.cloudfront.net/Mobile-Navigation/Mexico/About_MX_FamilyShot_740x455.jpg?mtime=20151203164249"
                alt="About MX FamilyShot 740x455" class="sidenav-image" data-pin-description="Truvía®">
            <ul class="nav navbar-nav navbar-main">
                <li><a href="//www.truvia.mx/acerca" title="Acerca de Truvía®">Nosotros</a></li>
                <li><a href="//www.truvia.mx/recetas" title="Recipes">Recetas</a></li>
                <li><a href="//www.truvia.mx/productos" title="Truvia Products: Reviews, Locator &amp; Partners">Producto</a></li>
            </ul>
        </div>
    </div>
    <!---->
    <div class="offcanvas-sidenav-sibling">

<!--header-->
        <header id="header">
            <nav class="navbar navbar-default">

            	<!--container-->
                <div class="container">
                	<!---->
                    <div class="navbar-header">
                        <button id="toggle" type="button" class="navbar-toggle collapsed" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a class="navbar-brand" href="//www.truvia.mx/">
                            <img height="41" src="images/truvia_logo_navbar.png" alt="Truvía®" data-pin-description="Truvía®">
                        </a>
                    </div>
                    <!---->
					<!---->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-main">
                            <li>
                                <a href="//www.truvia.mx/acerca" title="Acerca de Truvía®">Nosotros</a>
                            </li>
                            <li>
                                <a href="//www.truvia.mx/recetas" title="Recipes">Recetas</a>
                            </li>
                            <li>
                                <a href="//www.truvia.mx/productos" title="Truvia Products: Reviews, Locator &amp; Partners">Producto</a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle es_mx" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">región</a>
                                <ul class="truvia-bold dropdown-menu languages">
                                    <!-- Lanugage switcher -->
                                    <li>
                                        <a href="//www.truvia.com/canada/" class="en_ca">Canada (English)</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.com/canada-fr/" class="fr_ca">Canada (Français)</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.com.es/" class="es_es">España</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.fr/" class="fr_fr">Français</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.it/" class="it_it">Italia</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.mx/" class="es_mx">Mexico</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.co.uk/" class="en_gb">United Kingdom</a>
                                    </li>
                                    <li>
                                        <a href="//www.truvia.com/" class="en_us">United States</a>
                                    </li>
                                    <li>
                                        <a href="//truvia.com.ve" class="es_ve" target="_blank">Venezuela</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <!---->

                </div>
                <!--container-->
            </nav>
        </header>
<!--header-->



        <!--Main-->

        <main id="content" role="main">
            <div class="page-wrapper top-gradient">
                <div class="container">
                    <div class="row content-block--simpleText">
                        <div class="col-xs-12">
                            <h1 class="text-center">404 - Página no encontrada</h1>
                        </div>
                    </div>
                    <div class="row content-block--simpleText">
                        <div class="col-xs-12">
                            <p class="text-center large">Bueno, eso no es muy dulce. No hemos encontrado la página que está buscando. ¿Necesitas ayuda?
                                Póngase en contacto con nosotros <a href="//www.truvia.mx/contacto">aquí</a>.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="bottom-gradient"></div>
            </div>
        </main>

        <!--Main-->












		<!--footer-->
       
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <span class="hidden-xs">
                <!---->
                <div class="col-sm-4 col-md-2">
			        <ul class="footer__nav">
			            <li class="category"><a class="category__parent-link" href="//www.truvia.mx/acerca">Acerca de Truvía<sup>®</sup></a>
			                <ul class="category--children">
			                    <li class="category__child"><a href="//www.truvia.mx/acerca#stevia">STEVIA</a></li>
			                    <li class="category__child"><a href="//www.truvia.mx/acerca/compania">COMPAÑIA</a></li>
			                    <li class="category__child"><a href="//www.truvia.mx/acerca/compania#media">PRENSA</a></li>
			                </ul>
			            </li>
			        </ul>
			    </div>
			    <!---->
				<!---->
				<div class="col-sm-4 col-md-2">
			        <ul class="footer__nav">
			            <li class="category"><a class="category__parent-link" href="//www.truvia.mx/recetas">Recetas</a>
			                <ul class="category--children">
			                    <li class="category__child"><a href="//www.truvia.mx/tabla-de-conversion">TABLAS DE CONVERSIÓN</a></li>
			                </ul>
			            </li>
			        </ul>
			    </div>
				<!---->










    <div class="col-sm-4 col-md-2">
        <ul class="footer__nav">
            <li class="category">
                        <a class="category__parent-link" href="//www.truvia.mx/productos">Producto</a>
                    </li>
                </ul>
            </div>





            <div class="col-sm-4 col-md-2">
                <ul class="footer__nav">
                    <li class="category">
                        <a class="category__parent-link" href="//www.truvia.mx/dietetico">Cuidados Profesionales</a>
                        <ul class="category--children">
                            <li class="category__child"><a href="//www.truvia.mx/dietetico">INFORMACIÓN DIETÉTICA</a></li>
                            <li class="category__child"><a href="//www.truvia.mx/dietetico/investigacion">Investigación y Resultados</a></li>
                            <li class="category__child"><a href="//www.truvia.mx/dietetico/qa">FAQ</a></li>
                        </ul>
                    </li>
                </ul>
            </div>




            <div class="col-sm-4 col-md-2">
                <ul class="footer__nav"></ul>
    		</div>






        </span>
                    <div class="col-sm-4 col-md-2">
                        <ul class="footer__nav--last">
                            <li class="category--last">
                                <a class="category__parent-link" href="//www.truvia.mx/faq">FAQ</a>
                            </li>
                            <li class="category--last">
                                <a class="category__parent-link" href="//www.truvia.mx/contacto">Contacto</a>
                            </li>
                        </ul>
                    </div>


                    <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4 col-md-offset-0 col-md-2 vs-sm-top-4 vs-md-top-0">
                        <ul>
                            <li class="footer__social">
                                <span class="hidden-xs">Síguenos:</span>
                                <ul>
                                    <li>
                                        <a data-label="" href="https://www.facebook.com/TruviaMx" target="_blank"><img class="no-drag" src="//d2sf2a832xqb4v.cloudfront.net/Footer-Social/footer-facebook-icon.png?mtime=20151217145943"
                                                alt="footer-facebook-icon"></a>
                                    </li>
                                    <li>
                                        <a data-label="" href="https://twitter.com/truviamx" target="_blank"><img class="no-drag" src="//d2sf2a832xqb4v.cloudfront.net/Footer-Social/footer-twitter-icon.png?mtime=20151217150902"
                                                alt="footer-twitter-icon"></a>
                                    </li>
                                    <li>
                                        <a data-label="" href="https://www.pinterest.com/truviamx/" target="_blank"><img class="no-drag" src="//d2sf2a832xqb4v.cloudfront.net/Footer-Social/footer-pinterest-icon.png?mtime=20151217150449"
                                                alt="footer-pinterest-icon"></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>





                </div><!--144-->

                <div class="row">
                    <div class="col-sm-12">
                        <ul class="footer__secondary">
                            <li><a href="//www.truvia.mx/privacidad" title="Política de Privacidad ">Políticas de Privacidad </a></li>
                            <li><a href="//www.truvia.mx/terminos" title="Términos de Uso">Términos de Uso</a></li>
                            <li><a href="http://www.cargill.com" title="Cargill" target="_blank">Cargill</a></li>
                            <li class="hidden-sm hidden-xs"><span>©2016 The Truvía<sup>®</sup> Company, LLC</span></li>
                        </ul>
                    </div>
                </div>




            </div><!--container-->
        </footer>







        <!--</div>-->
        <div id="theModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <div class="modal__dynamic-content"></div>
            </div>

        </div>
    </div>



</body>
</html>